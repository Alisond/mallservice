package com.microMallApp.microMall.mall.domain.usecase;

import com.microMallApp.microMall.mall.domain.api.IDishServicePort;
import com.microMallApp.microMall.mall.domain.model.Dish;
import com.microMallApp.microMall.mall.domain.spi.IDishPersistencePort;

import java.util.List;

public class DishUseCase implements IDishServicePort {

    IDishPersistencePort dishPersistencePort;

    public DishUseCase(IDishPersistencePort dishPersistencePort) {
        this.dishPersistencePort = dishPersistencePort;
    }

    @Override
    public void createDish(Dish dish) {
        dishPersistencePort.createDish(dish);
    }

    @Override
    public void updateDish(Dish dish) {
        dishPersistencePort.updateDish(dish);
    }

    @Override
    public Dish getDishById(Long dishId) {
        return dishPersistencePort.getDishById(dishId);
    }

    @Override
    public void changeDishStatusById (Long dishId, Boolean status) {
        Dish actualDish = dishPersistencePort.getDishById(dishId);
        actualDish.setStatus(status);
        dishPersistencePort.updateDish(actualDish);
    }

    @Override
    public List<Dish> getAllDishesByRestaurant(Long restaurantId) {
        return dishPersistencePort.getAllDishesByRestaurant(restaurantId);
    }

    @Override
    public void deleteDish(Long dishId) {
        dishPersistencePort.deleteDish(dishId);
    }
}
