package com.microMallApp.microMall.mall.domain.spi;

import com.microMallApp.microMall.mall.domain.model.Restaurant;

import java.util.List;

public interface IRestaurantPersistencePort {

    void createRestaurant (Restaurant restaurant);
    Restaurant getRestaurant (Long restaurantId);
    void updateRestaurant (Restaurant restaurant);
    List<Restaurant> getRestaurants();
    List<Restaurant> getRestaurantsByOwner(Long ownerId);
    void deleteRestaurant (Long restaurantId);
}
