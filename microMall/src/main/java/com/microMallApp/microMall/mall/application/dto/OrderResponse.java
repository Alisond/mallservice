package com.microMallApp.microMall.mall.application.dto;

import com.microMallApp.microMall.mall.domain.model.Dish;
import java.util.List;


public class OrderResponse {

    private Long id;
    private String status; // default value for all orders CREATED
    private List<Dish> selectedDishes;
    private Long restaurantId;
    private Long employeeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Dish> getSelectedDishes() {
        return selectedDishes;
    }

    public void setSelectedDishes(List<Dish> selectedDishes) {
        this.selectedDishes = selectedDishes;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }
}
