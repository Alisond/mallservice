package com.microMallApp.microMall.mall.application.handler;

import com.microMallApp.microMall.mall.application.dto.DishRequest;
import com.microMallApp.microMall.mall.application.dto.DishResponse;
import com.microMallApp.microMall.mall.application.mapper.IDishMapper;
import com.microMallApp.microMall.mall.domain.api.IDishServicePort;
import com.microMallApp.microMall.mall.domain.model.Dish;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DishHandler implements IDishHandler{

    private final IDishServicePort dishServicePort;
    private final IDishMapper dishMapper;

    public DishHandler(IDishServicePort dishServicePort, IDishMapper dishMapper) {
        this.dishServicePort = dishServicePort;
        this.dishMapper = dishMapper;
    }

    @Override
    public void createDish(DishRequest dishRequest) {
        Dish dish = dishMapper.toDish(dishRequest);
        dishServicePort.createDish(dish);
    }

    @Override
    public void updateDish(DishRequest dishRequest) {
       Dish actualDish = dishServicePort.getDishById(dishRequest.getId());
       Dish newDish = dishMapper.toDish(dishRequest);
       newDish.setId(actualDish.getId());
       newDish.setStatus(actualDish.getStatus());
       newDish.setPrice(actualDish.getPrice());
       newDish.setDescription(actualDish.getDescription());
       dishServicePort.updateDish(newDish);
    }

    @Override
    public DishResponse getDishById(Long dishId) {
        Dish actualDish = dishServicePort.getDishById(dishId);
        return dishMapper.toResponse(actualDish);
    }

    @Override
    public DishResponse changeDishStatusById(Long dishId, Boolean status) {
        Dish actualDish = dishServicePort.getDishById(dishId);
        actualDish.setStatus(status);
        return dishMapper.toResponse(actualDish);
    }

    @Override
    public List<DishResponse> getAllDishesByRestaurant(Long restaurantId) {
        List<Dish> dishesByRestaurant = dishServicePort.getAllDishesByRestaurant(restaurantId);
        return dishMapper.toResponseList(dishesByRestaurant);
    }

    @Override
    public void deleteDish(Long dishId) {
        dishServicePort.deleteDish(dishId);
    }
}
