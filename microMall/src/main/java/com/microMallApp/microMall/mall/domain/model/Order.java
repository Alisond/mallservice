package com.microMallApp.microMall.mall.domain.model;

import java.util.List;

public class Order {

    private Long id;
    private String status; // default value for all orders CREATED
    private List<Dish> selectedDishes;
    private Long restaurantId;
    private Long employeeId;

    public Order(Long id, String status, List<Dish> selectedDishes, Long restaurantId, Long employeeId) {
        this.id = id;
        this.status = status;
        this.selectedDishes = selectedDishes;
        this.restaurantId = restaurantId;
        this.employeeId = employeeId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Dish> getSelectedDishes() {
        return selectedDishes;
    }

    public void setSelectedDishes(List<Dish> selectedDishes) {
        this.selectedDishes = selectedDishes;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }
}
