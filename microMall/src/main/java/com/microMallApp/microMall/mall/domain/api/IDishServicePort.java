package com.microMallApp.microMall.mall.domain.api;

import com.microMallApp.microMall.mall.domain.model.Dish;
import com.microMallApp.microMall.mall.domain.model.Restaurant;

import java.util.List;

public interface IDishServicePort {

    void createDish (Dish dish);
    void updateDish (Dish dish);  //only description and price
    Dish getDishById (Long dishId);
    void changeDishStatusById (Long dishId, Boolean status);
    List<Dish> getAllDishesByRestaurant (Long restaurantId);
    void deleteDish (Long dishId);

}
