package com.microMallApp.microMall.mall.domain.api;

import com.microMallApp.microMall.mall.domain.model.Dish;
import com.microMallApp.microMall.mall.domain.model.Order;
import com.microMallApp.microMall.mall.domain.model.Restaurant;

import java.util.List;

public interface IOrderServicePort {

    void createOrder (Order order);
    void updateDish (Order order);
    List<Order> getOrdersByRestaurant (Long restaurantId);
    List<Order> getOrdersByStatusAndRestaurant (String status, Long restaurantId);
    void deleteOrder (Long orderId);
}
