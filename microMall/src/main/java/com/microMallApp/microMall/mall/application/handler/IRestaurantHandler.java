package com.microMallApp.microMall.mall.application.handler;

import com.microMallApp.microMall.mall.application.dto.RestaurantRequest;
import com.microMallApp.microMall.mall.application.dto.RestaurantResponse;

import java.util.List;

public interface IRestaurantHandler {

    void createRestaurant (RestaurantRequest restaurantRequest);
    List<RestaurantResponse> getRestaurantsByOwner(Long ownerId);
    List<RestaurantResponse> getAllRestaurants();
}
