package com.microMallApp.microMall.mall.domain.spi;

import com.microMallApp.microMall.mall.domain.model.Dish;

import java.util.List;

public interface IDishPersistencePort {

    void createDish (Dish dish);
    Dish getDishById (Long dishId);
    void updateDish (Dish dish);  //only description and price
    List<Dish> getAllDishesByRestaurant (Long restaurantId);
    void deleteDish(Long dishId);

}
