package com.microMallApp.microMall.mall.infrastructure.exception.restaurant;

public class RestaurantNotFoundException extends RuntimeException{

    public RestaurantNotFoundException() {
        super();
    }
}
