package com.microMallApp.microMall.mall.infrastructure.output.jpa.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table (name="dish")
public class DishEntity {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "price", nullable = false)
    private Integer price;
    @Column(name = "description", nullable = false)
    private String description;
    @Column(name = "img_url", nullable = false)
    private String imgURL;
    @Column(name = "category", nullable = false)
    private String category;
    @Column(name = "status", nullable = false, columnDefinition = "boolean default TRUE")
    private Boolean status; //default for new dishes Active = true;
    @Column(name = "restaurant_id", nullable = false)
    private Long restaurantId;
    @ManyToMany
    private List<OrderEntity> orders;
}
