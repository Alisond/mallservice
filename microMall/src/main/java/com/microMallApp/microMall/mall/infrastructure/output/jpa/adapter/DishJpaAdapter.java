package com.microMallApp.microMall.mall.infrastructure.output.jpa.adapter;

import com.microMallApp.microMall.mall.domain.model.Dish;
import com.microMallApp.microMall.mall.domain.spi.IDishPersistencePort;
import com.microMallApp.microMall.mall.infrastructure.exception.dish.DishNotFoundException;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.entity.DishEntity;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.mapper.IDishEntityMapper;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.repository.IDishRepository;

import java.util.List;

public class DishJpaAdapter implements IDishPersistencePort {

    private final IDishRepository dishRepository;
    private final IDishEntityMapper dishEntityMapper;

    public DishJpaAdapter(IDishRepository dishRepository, IDishEntityMapper dishEntityMapper) {
        this.dishRepository = dishRepository;
        this.dishEntityMapper = dishEntityMapper;
    }

    @Override
    public void createDish(Dish dish) {
        dishRepository.save(dishEntityMapper.toEntity(dish));
    }

    @Override
    public Dish getDishById(Long dishId) {
        return dishEntityMapper.toDish(dishRepository.findById(dishId).orElseThrow(DishNotFoundException::new));
    }

    @Override
    public void updateDish(Dish dish) {
        dishRepository.save(dishEntityMapper.toEntity(dish));
    }

    @Override
    public List<Dish> getAllDishesByRestaurant(Long restaurantId) {
        List<DishEntity> dishEntityList = dishRepository.findByRestaurantId(restaurantId);
        if(dishEntityList.isEmpty()){
            throw new DishNotFoundException();
        }
        return dishEntityMapper.toDishList(dishEntityList);
    }

    @Override
    public void deleteDish(Long dishId) {
        dishRepository.deleteById(dishId);
    }
}
