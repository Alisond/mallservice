package com.microMallApp.microMall.mall.infrastructure.output.jpa.entity;
import com.microMallApp.microMall.mall.domain.model.Order;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table (name="orders")
public class OrderEntity {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "status", nullable = false)
    private String status; // default value for all orders CREATED
    @Column(name = "restaurant_id", nullable = false)
    private Long restaurantId;
    @Column(name = "employee_id", nullable = false)
    private Long employeeId;
    @ManyToMany
    private List<DishEntity> selectedDishes;
}
