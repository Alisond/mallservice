package com.microMallApp.microMall.mall.domain.usecase;

import com.microMallApp.microMall.mall.domain.api.IRestaurantServicePort;
import com.microMallApp.microMall.mall.domain.model.Restaurant;
import com.microMallApp.microMall.mall.domain.spi.IRestaurantPersistencePort;

import java.util.List;

public class RestaurantUseCase implements IRestaurantServicePort {

    IRestaurantPersistencePort restaurantPersistencePort;

    public RestaurantUseCase(IRestaurantPersistencePort restaurantPersistencePort) {
        this.restaurantPersistencePort = restaurantPersistencePort;
    }

    @Override
    public void createRestaurant(Restaurant restaurant) {
        restaurantPersistencePort.createRestaurant(restaurant);
    }

    @Override
    public List<Restaurant> getRestaurantsByOwner (Long ownerId){
        return restaurantPersistencePort.getRestaurantsByOwner(ownerId);
    };
    @Override
    public List<Restaurant> getAllRestaurants() {
        return restaurantPersistencePort.getRestaurants();
    }
}
