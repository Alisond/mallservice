package com.microMallApp.microMall.mall.infrastructure.output.jpa.mapper;

import com.microMallApp.microMall.mall.domain.model.Order;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.entity.OrderEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface IOrderEntityMapper {
    OrderEntity toEntity (Order order);
    Order toOrder (OrderEntity orderEntity);
    List<Order> toOrderList (List<OrderEntity> orderEntityList);
}
