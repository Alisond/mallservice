package com.microMallApp.microMall.mall.domain.api;

import com.microMallApp.microMall.mall.domain.model.Restaurant;

import java.util.List;

public interface IRestaurantServicePort {

    void createRestaurant (Restaurant restaurant);
    List<Restaurant> getRestaurantsByOwner(Long ownerId);
    List<Restaurant> getAllRestaurants();
}
