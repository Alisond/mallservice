package com.microMallApp.microMall.mall.application.mapper;

import com.microMallApp.microMall.mall.application.dto.DishRequest;
import com.microMallApp.microMall.mall.application.dto.DishResponse;
import com.microMallApp.microMall.mall.domain.model.Dish;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper
public interface IDishMapper {

    DishResponse toResponse (Dish dish);
    Dish toDish (DishRequest dishRequest);
    List<DishResponse> toResponseList (List<Dish> dishList);
}
