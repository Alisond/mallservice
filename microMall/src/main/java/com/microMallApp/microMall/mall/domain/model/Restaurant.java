package com.microMallApp.microMall.mall.domain.model;

public class Restaurant {

    private Long id;
    private Integer nit;
    private String name;
    private String address;
    private Integer phone;
    private String logoURL;
    private Long ownerId;

    public Restaurant(Long id, Integer nit, String name, String address, Integer phone, String logoURL, Long ownerId) {
        this.id = id;
        this.nit = nit;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.logoURL = logoURL;
        this.ownerId = ownerId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNit() {
        return nit;
    }

    public void setNit(Integer nit) {
        this.nit = nit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }
}


