package com.microMallApp.microMall.mall.infrastructure.output.jpa.repository;

import com.microMallApp.microMall.mall.domain.model.Order;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.entity.OrderEntity;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.entity.RestaurantEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IOrderRepository extends JpaRepository<OrderEntity,Long> {
    List<OrderEntity> findByRestaurantId (Long restaurantId);
    List<OrderEntity> findByStatusAndRestaurantId (String status, Long restaurantId);
}
