package com.microMallApp.microMall.mall.infrastructure.exception.dish;

public class DishAlreadyExistsException extends RuntimeException{

    public DishAlreadyExistsException() {
        super();
    }
}
