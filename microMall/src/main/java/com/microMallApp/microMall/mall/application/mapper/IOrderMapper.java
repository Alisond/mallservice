package com.microMallApp.microMall.mall.application.mapper;

import com.microMallApp.microMall.mall.application.dto.OrderRequest;
import com.microMallApp.microMall.mall.application.dto.OrderResponse;
import com.microMallApp.microMall.mall.domain.model.Order;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface IOrderMapper {
    OrderResponse toResponse (Order order);
    Order toOrder (OrderRequest orderRequest);
    List<OrderResponse> toResponseList (List<Order> orderList);
}
