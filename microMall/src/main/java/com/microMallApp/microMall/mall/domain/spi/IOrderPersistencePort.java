package com.microMallApp.microMall.mall.domain.spi;

import com.microMallApp.microMall.mall.domain.model.Order;
import com.microMallApp.microMall.mall.domain.model.Restaurant;

import java.util.List;

public interface IOrderPersistencePort {
    void createOrder (Order order);
    Order getOrder (Long orderId);
    void updateOrder (Order order);
    List<Order> getOrdersByRestaurant (Long restaurantId);
    List<Order> getOrdersByStatusAndRestaurant (String status, Long restaurantId);
    void deleteOrder (Long orderId);
}
