package com.microMallApp.microMall.mall.infrastructure.output.jpa.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name="restaurant")
public class RestaurantEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "nit", nullable = false)
    private Integer nit;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "address", nullable = false)
    private String address;
    @Column(name = "phone", nullable = false)
    private Integer phone;
    @Column(name = "logo_url", nullable = false)
    private String logoURL;
    @Column(name = "owner_id", nullable = false)
    private Long ownerId;

}
