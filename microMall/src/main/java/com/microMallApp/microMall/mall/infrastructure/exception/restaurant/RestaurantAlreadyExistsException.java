package com.microMallApp.microMall.mall.infrastructure.exception.restaurant;

public class RestaurantAlreadyExistsException extends RuntimeException{
    public RestaurantAlreadyExistsException(){
        super();
    }
}
