package com.microMallApp.microMall.mall.infrastructure.output.jpa.adapter;

import com.microMallApp.microMall.mall.domain.model.Restaurant;
import com.microMallApp.microMall.mall.domain.spi.IRestaurantPersistencePort;
import com.microMallApp.microMall.mall.infrastructure.exception.NoDataFoundException;
import com.microMallApp.microMall.mall.infrastructure.exception.restaurant.RestaurantNotFoundException;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.entity.RestaurantEntity;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.mapper.IRestaurantEntityMapper;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.repository.IRestaurantRepository;

import java.util.List;
import java.util.Optional;

public class RestaurantJpaAdapter implements IRestaurantPersistencePort {

    private final IRestaurantRepository restaurantRepository;
    private final IRestaurantEntityMapper restaurantEntityMapper;

    public RestaurantJpaAdapter(IRestaurantRepository restaurantRepository, IRestaurantEntityMapper restaurantEntityMapper) {
        this.restaurantRepository = restaurantRepository;
        this.restaurantEntityMapper = restaurantEntityMapper;
    }

    @Override
    public void createRestaurant(Restaurant restaurant) {
        restaurantRepository.save(restaurantEntityMapper.toEntity(restaurant));
    }

    @Override
    public Restaurant getRestaurant(Long restaurantId) {
        return restaurantEntityMapper.toRestaurant(restaurantRepository.findById(restaurantId).orElseThrow(RestaurantNotFoundException::new));
    }

    @Override
    public void updateRestaurant(Restaurant restaurant) {
        restaurantRepository.save(restaurantEntityMapper.toEntity(restaurant));
    }

    @Override
    public List<Restaurant> getRestaurants() {
        List<RestaurantEntity> restaurantEntityList = restaurantRepository.findAll();
        if(restaurantEntityList.isEmpty()){
            throw new NoDataFoundException();
        }
        return restaurantEntityMapper.toRestaurantList(restaurantEntityList);
    }

    @Override
    public List<Restaurant> getRestaurantsByOwner(Long ownerId) {
        List<RestaurantEntity> restaurantEntityList = restaurantRepository.findByOwnerId(ownerId);
        if(restaurantEntityList.isEmpty()){
            throw new RestaurantNotFoundException();
        }
        return restaurantEntityMapper.toRestaurantList(restaurantEntityList);
    }

    @Override
    public void deleteRestaurant(Long restaurantId) {
        restaurantRepository.deleteById(restaurantId);
    }
}
