package com.microMallApp.microMall.mall.application.handler;

import com.microMallApp.microMall.mall.application.dto.OrderRequest;
import com.microMallApp.microMall.mall.application.dto.OrderResponse;

import java.util.List;

public interface IOrderHandler {

    void createOrder (OrderRequest orderRequest);
    List<OrderResponse> getOrdersByRestaurant (Long restaurantId);
    List<OrderResponse> getOrdersByStatusAndRestaurant (String status, Long restaurantId);
    void deleteOrder (Long orderId);
}
