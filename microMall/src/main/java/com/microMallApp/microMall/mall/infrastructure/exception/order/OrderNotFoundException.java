package com.microMallApp.microMall.mall.infrastructure.exception.order;

public class OrderNotFoundException extends RuntimeException{

    public OrderNotFoundException() {
        super();
    }
}
