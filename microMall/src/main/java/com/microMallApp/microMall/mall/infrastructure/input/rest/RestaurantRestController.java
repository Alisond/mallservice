package com.microMallApp.microMall.mall.infrastructure.input.rest;

import com.microMallApp.microMall.mall.application.dto.RestaurantRequest;
import com.microMallApp.microMall.mall.application.dto.RestaurantResponse;
import com.microMallApp.microMall.mall.application.handler.IRestaurantHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1",method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH})
public class RestaurantRestController {
    protected static final Logger logger = LogManager.getLogger();
    private final IRestaurantHandler restaurantHandler;

    public RestaurantRestController(IRestaurantHandler restaurantHandler) {
        this.restaurantHandler = restaurantHandler;
    }

    @PostMapping("/restaurant")
    public ResponseEntity<Void> saveRestaurant (@RequestBody RestaurantRequest restaurantRequest){
        System.out.println("UserRestController");
        System.out.println(restaurantRequest.getId());
        restaurantHandler.createRestaurant(restaurantRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @GetMapping("/restaurants")
    public ResponseEntity<List<RestaurantResponse>> getRestaurants(){
        return ResponseEntity.ok(restaurantHandler.getAllRestaurants());
    }

}
