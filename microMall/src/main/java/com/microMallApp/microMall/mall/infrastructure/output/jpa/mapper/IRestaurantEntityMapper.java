package com.microMallApp.microMall.mall.infrastructure.output.jpa.mapper;

import com.microMallApp.microMall.mall.domain.model.Restaurant;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.entity.RestaurantEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface IRestaurantEntityMapper {
    RestaurantEntity toEntity (Restaurant restaurant);
    Restaurant toRestaurant (RestaurantEntity restaurantEntity);

    List<Restaurant> toRestaurantList (List<RestaurantEntity> restaurantEntityList);
}
