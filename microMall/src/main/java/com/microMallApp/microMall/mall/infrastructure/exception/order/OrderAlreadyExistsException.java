package com.microMallApp.microMall.mall.infrastructure.exception.order;

public class OrderAlreadyExistsException extends RuntimeException{

    public OrderAlreadyExistsException() {
        super();
    }
}
