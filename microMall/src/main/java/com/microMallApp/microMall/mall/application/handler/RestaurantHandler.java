package com.microMallApp.microMall.mall.application.handler;

import com.microMallApp.microMall.mall.application.dto.RestaurantRequest;
import com.microMallApp.microMall.mall.application.dto.RestaurantResponse;
import com.microMallApp.microMall.mall.application.mapper.IRestaurantMapper;
import com.microMallApp.microMall.mall.domain.api.IRestaurantServicePort;
import com.microMallApp.microMall.mall.domain.model.Restaurant;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RestaurantHandler implements IRestaurantHandler{

    private final IRestaurantServicePort restaurantServicePort;
    private final IRestaurantMapper restaurantMapper;

    public RestaurantHandler(IRestaurantServicePort restaurantServicePort, IRestaurantMapper restaurantMapper) {
        this.restaurantServicePort = restaurantServicePort;
        this.restaurantMapper = restaurantMapper;
    }

    @Override
    public void createRestaurant(RestaurantRequest restaurantRequest) {
        Restaurant restaurant = restaurantMapper.toRestaurant(restaurantRequest);
        restaurantServicePort.createRestaurant(restaurant);
    }

    @Override
    public List<RestaurantResponse> getRestaurantsByOwner(Long ownerId) {
        List<Restaurant> restaurantsByOwner = restaurantServicePort.getRestaurantsByOwner(ownerId);
        return restaurantMapper.toResponseList(restaurantsByOwner);
    }

    @Override
    public List<RestaurantResponse> getAllRestaurants() {
        List<Restaurant> getRestaurants = restaurantServicePort.getAllRestaurants();
        return restaurantMapper.toResponseList(getRestaurants);
    }
}
