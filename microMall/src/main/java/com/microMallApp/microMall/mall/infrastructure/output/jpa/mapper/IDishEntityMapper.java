package com.microMallApp.microMall.mall.infrastructure.output.jpa.mapper;

import com.microMallApp.microMall.mall.domain.model.Dish;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.entity.DishEntity;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper
public interface IDishEntityMapper {

    DishEntity toEntity (Dish dish);
    Dish toDish (DishEntity dishEntity);
    List<Dish> toDishList (List<DishEntity> dishEntityList);
}
