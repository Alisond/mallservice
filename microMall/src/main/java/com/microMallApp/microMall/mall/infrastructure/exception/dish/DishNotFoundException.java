package com.microMallApp.microMall.mall.infrastructure.exception.dish;

public class DishNotFoundException extends RuntimeException{

    public DishNotFoundException() {
        super();
    }
}
