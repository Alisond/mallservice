package com.microMallApp.microMall.mall.infrastructure.output.jpa.repository;

import com.microMallApp.microMall.mall.infrastructure.output.jpa.entity.DishEntity;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IDishRepository extends JpaRepository<DishEntity, Long> {
    List<DishEntity> findByRestaurantId (Long restaurantId);
}
