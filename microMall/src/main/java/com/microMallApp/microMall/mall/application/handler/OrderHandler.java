package com.microMallApp.microMall.mall.application.handler;

import com.microMallApp.microMall.mall.application.dto.OrderRequest;
import com.microMallApp.microMall.mall.application.dto.OrderResponse;
import com.microMallApp.microMall.mall.application.mapper.IOrderMapper;
import com.microMallApp.microMall.mall.domain.api.IOrderServicePort;
import com.microMallApp.microMall.mall.domain.model.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class OrderHandler implements IOrderHandler{

    private final IOrderServicePort orderServicePort;
    private final IOrderMapper orderMapper;

    public OrderHandler(IOrderServicePort orderServicePort, IOrderMapper orderMapper) {
        this.orderServicePort = orderServicePort;
        this.orderMapper = orderMapper;
    }

    @Override
    public void createOrder(OrderRequest orderRequest) {
        Order order = orderMapper.toOrder(orderRequest);
        orderServicePort.createOrder(order);
    }

    @Override
    public List<OrderResponse> getOrdersByRestaurant(Long restaurantId) {
        List<Order> OrdersByRestaurant = orderServicePort.getOrdersByRestaurant(restaurantId);
        return orderMapper.toResponseList(OrdersByRestaurant);
    }

    @Override
    public List<OrderResponse> getOrdersByStatusAndRestaurant(String status, Long restaurantId) {
        List<Order> ordersByStatusAndRestaurant = orderServicePort.getOrdersByStatusAndRestaurant(status, restaurantId);
        return orderMapper.toResponseList(ordersByStatusAndRestaurant);
    }

    @Override
    public void deleteOrder(Long orderId) {
        orderServicePort.deleteOrder(orderId);
    }
}
