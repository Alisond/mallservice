package com.microMallApp.microMall.mall.application.handler;

import com.microMallApp.microMall.mall.application.dto.DishRequest;
import com.microMallApp.microMall.mall.application.dto.DishResponse;

import java.util.List;

public interface IDishHandler {

    void createDish (DishRequest dishRequest);
    void updateDish (DishRequest dishRequest);  //only description and price
    DishResponse getDishById (Long dishId);
    DishResponse changeDishStatusById (Long dishId, Boolean status);
    List<DishResponse> getAllDishesByRestaurant (Long restaurantId);
    void deleteDish (Long dishId);
}
