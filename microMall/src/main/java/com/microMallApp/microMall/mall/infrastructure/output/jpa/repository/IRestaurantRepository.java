package com.microMallApp.microMall.mall.infrastructure.output.jpa.repository;

import com.microMallApp.microMall.mall.domain.model.Restaurant;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.entity.RestaurantEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface IRestaurantRepository extends JpaRepository<RestaurantEntity, Long> {
    List<RestaurantEntity> findByOwnerId (Long ownerId);
}
