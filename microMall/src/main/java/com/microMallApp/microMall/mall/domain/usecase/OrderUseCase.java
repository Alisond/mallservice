package com.microMallApp.microMall.mall.domain.usecase;

import com.microMallApp.microMall.mall.domain.api.IOrderServicePort;
import com.microMallApp.microMall.mall.domain.model.Order;
import com.microMallApp.microMall.mall.domain.model.Restaurant;
import com.microMallApp.microMall.mall.domain.spi.IOrderPersistencePort;

import java.util.List;

public class OrderUseCase implements IOrderServicePort {

    IOrderPersistencePort orderPersistencePort;

    public OrderUseCase(IOrderPersistencePort orderPersistencePort) {
        this.orderPersistencePort = orderPersistencePort;
    }

    @Override
    public void createOrder(Order order) {
        orderPersistencePort.createOrder(order);
    }

    @Override
    public void updateDish(Order order) {
        orderPersistencePort.updateOrder(order);
    }

    @Override
    public List<Order> getOrdersByRestaurant(Long restaurantId) {
        return orderPersistencePort.getOrdersByRestaurant(restaurantId);
    }

    @Override
    public List<Order> getOrdersByStatusAndRestaurant(String status, Long restaurantId) {
        return orderPersistencePort.getOrdersByStatusAndRestaurant(status,restaurantId);
    }

    @Override
    public void deleteOrder(Long orderId) {
        orderPersistencePort.deleteOrder(orderId);
    }
}
