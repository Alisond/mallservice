package com.microMallApp.microMall.mall.infrastructure.exception;

public class NoDataFoundException extends RuntimeException{

    public NoDataFoundException() {
        super();
    }
}
