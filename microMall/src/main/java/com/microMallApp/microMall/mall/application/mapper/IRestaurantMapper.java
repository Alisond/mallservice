package com.microMallApp.microMall.mall.application.mapper;

import com.microMallApp.microMall.mall.application.dto.RestaurantRequest;
import com.microMallApp.microMall.mall.application.dto.RestaurantResponse;
import com.microMallApp.microMall.mall.domain.model.Restaurant;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface IRestaurantMapper {
    RestaurantResponse toResponse (Restaurant restaurant);
    Restaurant toRestaurant (RestaurantRequest restaurantRequest);
    List<RestaurantResponse> toResponseList (List<Restaurant> restaurantList);
}
