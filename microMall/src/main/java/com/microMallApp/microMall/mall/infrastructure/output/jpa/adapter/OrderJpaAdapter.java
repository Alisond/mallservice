package com.microMallApp.microMall.mall.infrastructure.output.jpa.adapter;

import com.microMallApp.microMall.mall.domain.model.Order;
import com.microMallApp.microMall.mall.domain.spi.IOrderPersistencePort;
import com.microMallApp.microMall.mall.infrastructure.exception.dish.DishNotFoundException;
import com.microMallApp.microMall.mall.infrastructure.exception.order.OrderNotFoundException;
import com.microMallApp.microMall.mall.infrastructure.exception.restaurant.RestaurantNotFoundException;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.entity.OrderEntity;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.entity.RestaurantEntity;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.mapper.IOrderEntityMapper;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.repository.IOrderRepository;

import java.util.List;

public class OrderJpaAdapter implements IOrderPersistencePort {

    private final IOrderRepository orderRepository;
    private final IOrderEntityMapper orderEntityMapper;

    public OrderJpaAdapter(IOrderRepository orderRepository, IOrderEntityMapper orderEntityMapper) {
        this.orderRepository = orderRepository;
        this.orderEntityMapper = orderEntityMapper;
    }

    @Override
    public void createOrder(Order order) {
        orderRepository.save(orderEntityMapper.toEntity(order));
    }

    @Override
    public Order getOrder(Long orderId) {
        return orderEntityMapper.toOrder(orderRepository.findById(orderId).orElseThrow(OrderNotFoundException::new));
    }

    @Override
    public void updateOrder(Order order) {
        orderRepository.save(orderEntityMapper.toEntity(order));
    }

    @Override
    public List<Order> getOrdersByRestaurant(Long restaurantId) {
        List<OrderEntity> orderEntityList = orderRepository.findByRestaurantId(restaurantId);
        if(orderEntityList.isEmpty()){
            throw new OrderNotFoundException();
        }
        return orderEntityMapper.toOrderList(orderEntityList);
    }

    @Override
    public List<Order> getOrdersByStatusAndRestaurant(String status, Long restaurantId) {
        List<OrderEntity> ordersByStatusAndRestaurant = orderRepository.findByStatusAndRestaurantId(status, restaurantId);
        if(ordersByStatusAndRestaurant.isEmpty()){
            throw new OrderNotFoundException();
        }
        return orderEntityMapper.toOrderList(ordersByStatusAndRestaurant);
    }

    @Override
    public void deleteOrder(Long orderId) {
        orderRepository.deleteById(orderId);
    }
}
