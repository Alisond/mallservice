package com.microMallApp.microMall.mall.infrastructure.configuration;

import com.microMallApp.microMall.mall.domain.api.IDishServicePort;
import com.microMallApp.microMall.mall.domain.api.IOrderServicePort;
import com.microMallApp.microMall.mall.domain.api.IRestaurantServicePort;
import com.microMallApp.microMall.mall.domain.spi.IDishPersistencePort;
import com.microMallApp.microMall.mall.domain.spi.IOrderPersistencePort;
import com.microMallApp.microMall.mall.domain.spi.IRestaurantPersistencePort;
import com.microMallApp.microMall.mall.domain.usecase.DishUseCase;
import com.microMallApp.microMall.mall.domain.usecase.OrderUseCase;
import com.microMallApp.microMall.mall.domain.usecase.RestaurantUseCase;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.adapter.DishJpaAdapter;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.adapter.OrderJpaAdapter;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.adapter.RestaurantJpaAdapter;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.mapper.IDishEntityMapper;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.mapper.IOrderEntityMapper;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.mapper.IRestaurantEntityMapper;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.repository.IDishRepository;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.repository.IOrderRepository;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.repository.IRestaurantRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {
    private final IRestaurantRepository restaurantRepository;
    private final IOrderRepository orderRepository;
    private final IDishRepository dishRepository;

    private final IRestaurantEntityMapper restaurantEntityMapper;
    private final IOrderEntityMapper orderEntityMapper;
    private final IDishEntityMapper dishEntityMapper;

    public BeanConfiguration(IRestaurantRepository restaurantRepository, IOrderRepository orderRepository, IDishRepository dishRepository, IRestaurantEntityMapper restaurantEntityMapper, IOrderEntityMapper orderEntityMapper, IDishEntityMapper dishEntityMapper) {
        this.restaurantRepository = restaurantRepository;
        this.orderRepository = orderRepository;
        this.dishRepository = dishRepository;
        this.restaurantEntityMapper = restaurantEntityMapper;
        this.orderEntityMapper = orderEntityMapper;
        this.dishEntityMapper = dishEntityMapper;
    }

    @Bean
    public IRestaurantPersistencePort restaurantPersistencePort(){
        return new RestaurantJpaAdapter(restaurantRepository,restaurantEntityMapper);
    }
    @Bean
    public IRestaurantServicePort restaurantServicePort(){
        return new RestaurantUseCase(restaurantPersistencePort());
    }

    @Bean
    public IOrderPersistencePort orderPersistencePort(){
        return new OrderJpaAdapter(orderRepository,orderEntityMapper);
    }
    @Bean
    public IOrderServicePort orderServicePort(){
        return new OrderUseCase(orderPersistencePort());
    }

    @Bean
    public IDishPersistencePort dishPersistencePort(){
        return new DishJpaAdapter(dishRepository,dishEntityMapper);
    }
    @Bean
    public IDishServicePort dishServicePort(){
        return new DishUseCase(dishPersistencePort());
    }
}
