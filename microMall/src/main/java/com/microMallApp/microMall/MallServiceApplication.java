package com.microMallApp.microMall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication (scanBasePackages = {"controller","service","model"})
public class MallServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MallServiceApplication.class, args);
	}

}
