package com.microMallApp.microMall.mall.infrastructure.output.jpa.mapper;

import com.microMallApp.microMall.mall.domain.model.Dish;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.entity.DishEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-12-20T03:28:56-0500",
    comments = "version: 1.6.0.Beta1, compiler: javac, environment: Java 17.0.8 (Oracle Corporation)"
)
public class IDishEntityMapperImpl implements IDishEntityMapper {

    @Override
    public DishEntity toEntity(Dish dish) {
        if ( dish == null ) {
            return null;
        }

        DishEntity dishEntity = new DishEntity();

        return dishEntity;
    }

    @Override
    public Dish toDish(DishEntity dishEntity) {
        if ( dishEntity == null ) {
            return null;
        }

        Long id = null;
        String name = null;
        Integer price = null;
        String description = null;
        String imgURL = null;
        String category = null;
        Boolean status = null;
        Long restaurantId = null;

        Dish dish = new Dish( id, name, price, description, imgURL, category, status, restaurantId );

        return dish;
    }

    @Override
    public List<Dish> toDishList(List<DishEntity> dishEntityList) {
        if ( dishEntityList == null ) {
            return null;
        }

        List<Dish> list = new ArrayList<Dish>( dishEntityList.size() );
        for ( DishEntity dishEntity : dishEntityList ) {
            list.add( toDish( dishEntity ) );
        }

        return list;
    }
}
