package com.microMallApp.microMall.mall.infrastructure.output.jpa.mapper;

import com.microMallApp.microMall.mall.domain.model.Restaurant;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.entity.RestaurantEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-12-20T03:28:56-0500",
    comments = "version: 1.6.0.Beta1, compiler: javac, environment: Java 17.0.8 (Oracle Corporation)"
)
public class IRestaurantEntityMapperImpl implements IRestaurantEntityMapper {

    @Override
    public RestaurantEntity toEntity(Restaurant restaurant) {
        if ( restaurant == null ) {
            return null;
        }

        RestaurantEntity restaurantEntity = new RestaurantEntity();

        return restaurantEntity;
    }

    @Override
    public Restaurant toRestaurant(RestaurantEntity restaurantEntity) {
        if ( restaurantEntity == null ) {
            return null;
        }

        Long id = null;
        Integer nit = null;
        String name = null;
        String address = null;
        Integer phone = null;
        String logoURL = null;
        Long ownerId = null;

        Restaurant restaurant = new Restaurant( id, nit, name, address, phone, logoURL, ownerId );

        return restaurant;
    }

    @Override
    public List<Restaurant> toRestaurantList(List<RestaurantEntity> restaurantEntityList) {
        if ( restaurantEntityList == null ) {
            return null;
        }

        List<Restaurant> list = new ArrayList<Restaurant>( restaurantEntityList.size() );
        for ( RestaurantEntity restaurantEntity : restaurantEntityList ) {
            list.add( toRestaurant( restaurantEntity ) );
        }

        return list;
    }
}
