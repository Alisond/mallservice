package com.microMallApp.microMall.mall.infrastructure.output.jpa.mapper;

import com.microMallApp.microMall.mall.domain.model.Dish;
import com.microMallApp.microMall.mall.domain.model.Order;
import com.microMallApp.microMall.mall.infrastructure.output.jpa.entity.OrderEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-12-20T03:28:56-0500",
    comments = "version: 1.6.0.Beta1, compiler: javac, environment: Java 17.0.8 (Oracle Corporation)"
)
public class IOrderEntityMapperImpl implements IOrderEntityMapper {

    @Override
    public OrderEntity toEntity(Order order) {
        if ( order == null ) {
            return null;
        }

        OrderEntity orderEntity = new OrderEntity();

        return orderEntity;
    }

    @Override
    public Order toOrder(OrderEntity orderEntity) {
        if ( orderEntity == null ) {
            return null;
        }

        Long id = null;
        String status = null;
        List<Dish> selectedDishes = null;
        Long restaurantId = null;
        Long employeeId = null;

        Order order = new Order( id, status, selectedDishes, restaurantId, employeeId );

        return order;
    }

    @Override
    public List<Order> toOrderList(List<OrderEntity> orderEntityList) {
        if ( orderEntityList == null ) {
            return null;
        }

        List<Order> list = new ArrayList<Order>( orderEntityList.size() );
        for ( OrderEntity orderEntity : orderEntityList ) {
            list.add( toOrder( orderEntity ) );
        }

        return list;
    }
}
