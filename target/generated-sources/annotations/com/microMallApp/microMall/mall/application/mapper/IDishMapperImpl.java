package com.microMallApp.microMall.mall.application.mapper;

import com.microMallApp.microMall.mall.application.dto.DishRequest;
import com.microMallApp.microMall.mall.application.dto.DishResponse;
import com.microMallApp.microMall.mall.domain.model.Dish;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-12-20T03:28:56-0500",
    comments = "version: 1.6.0.Beta1, compiler: javac, environment: Java 17.0.8 (Oracle Corporation)"
)
public class IDishMapperImpl implements IDishMapper {

    @Override
    public DishResponse toResponse(Dish dish) {
        if ( dish == null ) {
            return null;
        }

        DishResponse dishResponse = new DishResponse();

        dishResponse.setId( dish.getId() );
        dishResponse.setName( dish.getName() );
        dishResponse.setPrice( dish.getPrice() );
        dishResponse.setDescription( dish.getDescription() );
        dishResponse.setImgURL( dish.getImgURL() );
        dishResponse.setCategory( dish.getCategory() );
        dishResponse.setStatus( dish.getStatus() );
        dishResponse.setRestaurantId( dish.getRestaurantId() );

        return dishResponse;
    }

    @Override
    public Dish toDish(DishRequest dishRequest) {
        if ( dishRequest == null ) {
            return null;
        }

        Long id = null;
        String name = null;
        Integer price = null;
        String description = null;
        String imgURL = null;
        String category = null;
        Boolean status = null;
        Long restaurantId = null;

        id = dishRequest.getId();
        name = dishRequest.getName();
        price = dishRequest.getPrice();
        description = dishRequest.getDescription();
        imgURL = dishRequest.getImgURL();
        category = dishRequest.getCategory();
        status = dishRequest.getStatus();
        restaurantId = dishRequest.getRestaurantId();

        Dish dish = new Dish( id, name, price, description, imgURL, category, status, restaurantId );

        return dish;
    }

    @Override
    public List<DishResponse> toResponseList(List<Dish> dishList) {
        if ( dishList == null ) {
            return null;
        }

        List<DishResponse> list = new ArrayList<DishResponse>( dishList.size() );
        for ( Dish dish : dishList ) {
            list.add( toResponse( dish ) );
        }

        return list;
    }
}
