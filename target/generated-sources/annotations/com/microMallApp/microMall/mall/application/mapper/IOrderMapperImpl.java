package com.microMallApp.microMall.mall.application.mapper;

import com.microMallApp.microMall.mall.application.dto.OrderRequest;
import com.microMallApp.microMall.mall.application.dto.OrderResponse;
import com.microMallApp.microMall.mall.domain.model.Dish;
import com.microMallApp.microMall.mall.domain.model.Order;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-12-20T03:28:56-0500",
    comments = "version: 1.6.0.Beta1, compiler: javac, environment: Java 17.0.8 (Oracle Corporation)"
)
public class IOrderMapperImpl implements IOrderMapper {

    @Override
    public OrderResponse toResponse(Order order) {
        if ( order == null ) {
            return null;
        }

        OrderResponse orderResponse = new OrderResponse();

        orderResponse.setId( order.getId() );
        orderResponse.setStatus( order.getStatus() );
        List<Dish> list = order.getSelectedDishes();
        if ( list != null ) {
            orderResponse.setSelectedDishes( new ArrayList<Dish>( list ) );
        }
        orderResponse.setRestaurantId( order.getRestaurantId() );
        orderResponse.setEmployeeId( order.getEmployeeId() );

        return orderResponse;
    }

    @Override
    public Order toOrder(OrderRequest orderRequest) {
        if ( orderRequest == null ) {
            return null;
        }

        Long id = null;
        String status = null;
        List<Dish> selectedDishes = null;
        Long restaurantId = null;
        Long employeeId = null;

        id = orderRequest.getId();
        status = orderRequest.getStatus();
        List<Dish> list = orderRequest.getSelectedDishes();
        if ( list != null ) {
            selectedDishes = new ArrayList<Dish>( list );
        }
        restaurantId = orderRequest.getRestaurantId();
        employeeId = orderRequest.getEmployeeId();

        Order order = new Order( id, status, selectedDishes, restaurantId, employeeId );

        return order;
    }

    @Override
    public List<OrderResponse> toResponseList(List<Order> orderList) {
        if ( orderList == null ) {
            return null;
        }

        List<OrderResponse> list = new ArrayList<OrderResponse>( orderList.size() );
        for ( Order order : orderList ) {
            list.add( toResponse( order ) );
        }

        return list;
    }
}
