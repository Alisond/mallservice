package com.microMallApp.microMall.mall.application.mapper;

import com.microMallApp.microMall.mall.application.dto.RestaurantRequest;
import com.microMallApp.microMall.mall.application.dto.RestaurantResponse;
import com.microMallApp.microMall.mall.domain.model.Restaurant;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-12-20T03:28:56-0500",
    comments = "version: 1.6.0.Beta1, compiler: javac, environment: Java 17.0.8 (Oracle Corporation)"
)
public class IRestaurantMapperImpl implements IRestaurantMapper {

    @Override
    public RestaurantResponse toResponse(Restaurant restaurant) {
        if ( restaurant == null ) {
            return null;
        }

        RestaurantResponse restaurantResponse = new RestaurantResponse();

        restaurantResponse.setId( restaurant.getId() );
        restaurantResponse.setNit( restaurant.getNit() );
        restaurantResponse.setName( restaurant.getName() );
        restaurantResponse.setAddress( restaurant.getAddress() );
        restaurantResponse.setPhone( restaurant.getPhone() );
        restaurantResponse.setLogoURL( restaurant.getLogoURL() );
        restaurantResponse.setOwnerId( restaurant.getOwnerId() );

        return restaurantResponse;
    }

    @Override
    public Restaurant toRestaurant(RestaurantRequest restaurantRequest) {
        if ( restaurantRequest == null ) {
            return null;
        }

        Long id = null;
        Integer nit = null;
        String name = null;
        String address = null;
        Integer phone = null;
        String logoURL = null;
        Long ownerId = null;

        id = restaurantRequest.getId();
        nit = restaurantRequest.getNit();
        name = restaurantRequest.getName();
        address = restaurantRequest.getAddress();
        phone = restaurantRequest.getPhone();
        logoURL = restaurantRequest.getLogoURL();
        ownerId = restaurantRequest.getOwnerId();

        Restaurant restaurant = new Restaurant( id, nit, name, address, phone, logoURL, ownerId );

        return restaurant;
    }

    @Override
    public List<RestaurantResponse> toResponseList(List<Restaurant> restaurantList) {
        if ( restaurantList == null ) {
            return null;
        }

        List<RestaurantResponse> list = new ArrayList<RestaurantResponse>( restaurantList.size() );
        for ( Restaurant restaurant : restaurantList ) {
            list.add( toResponse( restaurant ) );
        }

        return list;
    }
}
